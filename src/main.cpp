#include <Arduino.h>
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define TEMPERATURE_PRECISION 12

#define ONE_WIRE_BUS 13 // esp32: 23, esp8266: 13

const unsigned int meansureDelay = 2000;
unsigned int meansureTime = millis();

OneWire ow(ONE_WIRE_BUS);
DallasTemperature ds(&ow);
DeviceAddress adr_temp0, adr_temp1;

WiFiManager wm;

WiFiClient client;
PubSubClient mqttClient(client);

void setup()
{
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  bool res;
  res = wm.autoConnect();

  mqttClient.setServer("192.168.10.228", 1883);

  pinMode(LED_BUILTIN, OUTPUT);
  ds.begin();

  ds.getAddress(adr_temp0, 0);
  ds.getAddress(adr_temp1, 1);
  // ds.setResolution(adr_temp0, TEMPERATURE_PRECISION);
  // ds.setResolution(adr_temp1, TEMPERATURE_PRECISION);
  ds.requestTemperatures();

  meansureTime = millis() + meansureDelay;
}

void mqttReconnect()
{
  int retry = 0;
  while (!mqttClient.connected() || retry == 10)
  {
    Serial.print("Attempting MQTT connection...");
    retry++;
    if (mqttClient.connect("zwyc_03", "zwyc", "20apples"))
    {
      Serial.println("connected");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 10 seconds");
      // Wait 3 seconds before retrying
      delay(1000);
    }
  }
}

void updateMqtt(float ftemp0, float ftemp1)
{
  char tmpBuf[10] = "";
  char mqtt_topic[40] = "";

  if (!mqttClient.connected())
    mqttReconnect();

  if (mqttClient.connected())
  {
    //      StaticJsonDocument<capacity> json;

    dtostrf(ftemp0, 8, 2, tmpBuf);
    // json["temp0"] = tmpBuf;
    sprintf(mqtt_topic, "zwyc_03%s", "/temp/0");
    mqttClient.publish(mqtt_topic, tmpBuf);
    dtostrf(ftemp1, 8, 2, tmpBuf);
    // json["temp0"] = tmpBuf;
    sprintf(mqtt_topic, "zwyc_03%s", "/temp/1");
    mqttClient.publish(mqtt_topic, tmpBuf);
  }
}

void loop()
{
  float temp0, temp1;
  // put your main code here, to run repeatedly:

  // delay(2000);
  if(meansureTime < millis()) {
    meansureTime = millis() + meansureDelay;
      
    temp0 = ds.getTempC(adr_temp0);
    temp1 = ds.getTempC(adr_temp1);

    /*  Serial.print("temp0: ");
      Serial.print(temp1, 4);
      Serial.print("  temp1: ");
      Serial.println(temp0, 4);
    */
    ds.requestTemperatures();

    digitalWrite(LED_BUILTIN, LOW); // HIGH  for 8266: LOW
    updateMqtt(temp1, temp0);
    digitalWrite(LED_BUILTIN, HIGH); // LOW   for 8266: HIGH
  }
}
